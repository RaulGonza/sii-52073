El juego consiste en dos jugadores, cada uno con su raqueta con la que golpear una pelota e intentarán meter la pelota en el lado contrario de su adversario.
Los evento introducidos son:
- El radio de la pelota disminuye con el tiempo.
- La velocidad de la pelota aumenta con el tiempo.
-Cada 30 segundos aparece una nueva pelota en el juego.
