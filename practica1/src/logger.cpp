#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#define N 56
int main()
{
	int e_t; //error test	
	int fd; //descriptor de la fifo
	int fifo_error; //determina si hay error por lectura o cierre en la fifo
	
	const char* fifo = "/tmp/fifo";	//donde está el fifo(creada la ruta en mundo

	char buffer [N];

	if(mkfifo(fifo, 0777)!=0){//Fifo con permisos 
		perror("mkfifo");//error al crear
		exit(1);
	}

	fd = open(fifo, O_RDONLY);//abrimos fifo para leer
	if(fd==-1){//si hay error
		perror("open fifo");
		exit(1);
	}
	while(1){
		
		fifo_error = read(fd,&buffer,N*sizeof(char));
		
		if(fifo_error==-1){			
			perror("error read from fifo(logger), se cierra logger");
			exit(1);
		}
		else if(fifo_error==0){			
			perror("cerrada la fifo from client(logger), se cierra logger");
			exit(1);
		}
		fifo_error = write(1,buffer,N*sizeof(char)); 

		if(fifo_error == -1){
			perror("error write from fifo(logger), se cierra logger");
 			exit(1);
 		}


		else if(fifo_error==0){			
			perror("cerrada la fifo from client(logger), se cierra logger");
 			exit(1);
 		}
	}
	if(close(fd)!=0 || unlink(fifo)!=0){
		perror("close fifo (logger)");
		exit(1);
 	}
	return 0;
}
