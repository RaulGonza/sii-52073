// Mundo.cpp: implementation of the CMundo class.
//RAÚL GONZÁLEZ COSTUMERO 52073
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>//cerrar
#include <fcntl.h> //abrir

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <signal.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	//fifo="/tmp/fifo";
	fichero_compartido= "/tmp/fichero_compartido";
	fifo_from_server = "/tmp/fifo_from_server";		//fifo que viene del servidor
	fifo_from_client = "/tmp/fifo_from_client";		//fifo que viene del client
 	Init();
}

CMundo::~CMundo()
{
	pdmc->accion = 5; //se envia un 5 que hara al bot cerrarse
	if(unlink(fichero_compartido) !=0){
		perror("error cerrando fichero compartido server");
 		exit(1);
	}
	if(close(fd_ffms) != 0 || unlink(fifo_from_server) !=0){	//cerrar fifo from server
		perror("error cerrando fifo del server");
		exit(1);
	}
	if(close(fd_ffmc) != 0 || unlink(fifo_from_client) !=0){	//cerrar fifo from server
		perror("error cerrando fifo del cliente");
		exit(1);
}
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b){
	
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];

	sprintf(cad,"Jugador1: %d",puntos1);//1 rojo
	print(cad,10,0,1,0,0);
	sprintf(cad,"Jugador2: %d",puntos2);//2 azul
	print(cad,650,0,0,0,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	//esfera.Dibuja();

	for(int i=0;i<esferas.size();i++)
	{
		esferas[i]->Dibuja();
	}


	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);

	if(tiempo--<0)//añade nueva esfera 30seg
	{
		tiempo=1200;
		esferas.push_back(new Esfera);
	}

	for(int i=0;i<esferas.size();i++)
	{
		esferas[i]->Mueve(0.025f);
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);
		if(fondo_izq.Rebota(*esferas[i]))
		{
			for(int j=0;j<esferas.size();j++)
				delete esferas[j];
			esferas.clear();
			Esfera *e=new Esfera;
			e->centro.x=0;

			e->radio=0.5f;

			e->centro.y=rand()/(float)RAND_MAX;
			e->velocidad.x=2+2*rand()/(float)RAND_MAX;
			e->velocidad.y=2+2*rand()/(float)RAND_MAX;
			esferas.push_back(e);
			puntos2++;
			
			tiempo=1200;
	}

	if(fondo_dcho.Rebota(*esferas[i]))
	{
		for(int j=0;j<esferas.size();j++)
			delete esferas[j];
		esferas.clear();
		Esfera *e=new Esfera;
		e->centro.x=0;

		e->radio=0.5f;

		e->centro.y=rand()/(float)RAND_MAX;
		e->velocidad.x=-2-2*rand()/(float)RAND_MAX;
		e->velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
		puntos1++;
		
		tiempo=1200;
	}
}

	int i;
	for(i=0;i<paredes.size();i++)
	{
		//paredes[i].Rebota(esfera);
		for(int j=0;j<esferas.size();j++)
		{
			paredes[i].Rebota(*esferas[j]);
		}
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
//datos compartidos
	float aux1 = esferas[0]->centro.x;		//se calcula el indice de la esfera mas cercana al pala 1 y 2, que seran las que es intentaran parar

	int i_e1=0;
	for(int i = 0; i < esferas.size(); i++){
		if(esferas[i]->centro.x<aux1)	//para la pala 1 la mas a la izda
			i_e1=i;

	}
	float aux2 = esferas[0]->centro.x;
	int i_e2=0;
	for(int i = 0; i < esferas.size(); i++){
		if(esferas[i]->centro.x>aux2)	//para la pala 2 la mas a la dcha
			i_e2=i;
	}
	pdmc->esfera1.centro.x=esferas[i_e1]->centro.x;
	pdmc->esfera1.centro.y=esferas[i_e1]->centro.y;
	pdmc->esfera2.centro.x=esferas[i_e2]->centro.x;
	pdmc->esfera2.centro.y=esferas[i_e2]->centro.y;
	pdmc->raqueta1.y1=jugador1.y1;
	pdmc->raqueta1.y2=jugador1.y2;
	pdmc->raqueta2.y1=jugador2.y1;
	pdmc->raqueta2.y2=jugador2.y2;	
	if(pdmc->control_r1){		//si el bot esta controlando la pala 1
		if(pdmc->accion==1){
			keystates['w']=true;
			keystates['s']=false;
		}
		else if(pdmc->accion==-1){
			keystates['w']=false;
			keystates['s']=true;
		}
		else if(pdmc->accion==0){
			keystates['w']=false;
			keystates['s']=false;
		}
	}
	if(pdmc->control_r2){		//si el bot esta controlando la pala 2
		if(pdmc->accion2==1){
			keystates['o']=true;
			keystates['l']=false;
		}
		else if(pdmc->accion2==-1){
			keystates['o']=false;
			keystates['l']=true;
		}
		else if(pdmc->accion==0){
			keystates['o']=false;
			keystates['l']=false;
		}
	}
//fifo from server to cliente
	char cad_ffms[200];
	sprintf(cad_ffms,"%f %f %f %f %f %f %f %f %f %f %d %d", esferas[0]->centro.x,esferas[0]->centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	int ffms_error = write(fd_ffms,cad_ffms,sizeof(cad_ffms));	//se guarda el valor de retorno para comprobar errores
	if(ffms_error == -1){
		perror("error al escribir fifo server en el cliente");
		this->~CMundo();		
	}
	else if (ffms_error == 0){
		perror("se cierra fifo del servidor en el cliente");
		this->~CMundo();
	}
	//TERMINAMOS EL JUEGOS QUIEN LLEGA A 3
	if(puntos1==3||puntos2==3)
	{
	exit(1);
	}

}
void CMundo::Keyboard()
{
	if(keystates['w'])
		jugador1.velocidad.y=6;
	else if (keystates['s'])
		jugador1.velocidad.y=-6;
	else
		jugador1.velocidad.y=0;
	if(keystates['p'])
		jugador2.velocidad.y=6;
	else if (keystates['l'])
		jugador2.velocidad.y=-6;
	else
		jugador2.velocidad.y=0;
}
void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}
void CMundo::RecibeComandosJugador()
{
     int ffmct_error;
     while (1) {
            usleep(10);
            ffmct_error = read(fd_ffmc, keystates, sizeof(keystates));
	    if (ffmct_error == -1){
		perror("cerrar fifo del cliente, el thread se cierra");
		this->~CMundo();		
		//exit(1);
	    }
      }
}

void tratar_alarma(int n){		//tratamiento de señales
	if (n == SIGUSR1){
		perror("servidor terminado correctamente(estado de terminacion 0)(SIGUSR1)");
		exit(n);
	}	
	else{
		char cad_signal[256];
		sscanf(cad_signal,"servidor terminado por la senyal %d",&n);
		perror(cad_signal);
		exit(n);
	}
}

void CMundo::Init()
{
	//donde se va a guardar mis datos
	caddr_t dst; //direccion
	
	pthread_create(&thid1, NULL, hilo_comandos, this);

	tiempo=1200;
	esferas.push_back(new Esfera);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.setColor(0,0,1);
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.setColor(0,0,1);
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	jugador1.setColor(1,0,0);

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	jugador2.setColor(0,0,1);

	//Datos memoria compartida//
		if((fdc=open(fichero_compartido, O_CREAT|O_TRUNC|O_RDWR,0777))<0){
		perror("No puede crearse el fichero compartido");
		close(fdc);
		this->~CMundo();
	}
	if(ftruncate(fdc,sizeof(pdmc))<0){
		perror("error en ftruncate");
		close(fdc);
		this->~CMundo();
	}
	if((dst=(caddr_t)mmap((caddr_t)0, sizeof(pdmc), PROT_WRITE | PROT_READ, MAP_SHARED, fdc,0))== MAP_FAILED){
		perror("Error en la proyeccion del fichero compartido");
		close(fdc);
		this->~CMundo();
	}
	//Hay que cerrar fichero
	close(fdc);
	pdmc=(DatosMemCompartida*)dst;

//fifo from server
	fd_ffms = open(fifo_from_server,O_WRONLY);
	if(fd_ffms==-1){
		perror("error abrir fifo del server");
		this->~CMundo();
	}
	
	//fifo from client
	fd_ffmc = open(fifo_from_client,O_RDONLY);
	if(fd_ffmc==-1){
		perror("error abrir fifo del client");
		this->~CMundo();		
	}
	
	pthread_create(&thid1, NULL, hilo_comandos, this);
	
	//tratamiento de señales
	
	struct sigaction act;
	sigset_t mask;
	/* estable el manejador */
	act.sa_handler = tratar_alarma;
	/* función a ejecutar */
	act.sa_flags = SA_RESTART;
	/* ninguna acción específica*/
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT, &act,NULL);
	sigaction(SIGTERM, &act,NULL);
	sigaction(SIGPIPE, &act,NULL);
	sigaction(SIGUSR1, &act,NULL);
}
