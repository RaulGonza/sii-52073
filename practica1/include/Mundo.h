// Mundo.h: interface for the CMundo class.
//RAÚL GONZÁLEZ COSTUMERO 52073
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

#include "DatosMemCompartida.h"

class CMundo  
{
public:

	//Creamos el fifo con el identificador de la tuberia
	const char* fifo;
	int t;
	//
	//Memoria compartida
	const char* fichero_compartido;
	int fdc;
	//
	//Creamos donde compartir datos
	DatosMemCompartida dcm;
	DatosMemCompartida* pdmc;
	//
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	//void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void Keyboard();	

	std::vector<Esfera *> esferas;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	bool keystates[256];//vector saber si esta pulsado o no 
	int tiempo; //el contador
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
