// Raqueta.h: interface for the Raqueta class.
//RAÚL GONZÁLEZ COSTUMERO 52073
//////////////////////////////////////////////////////////////////////
#ifndef RAQUETA_INC
#define RAQUETA_INC
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
#endif
